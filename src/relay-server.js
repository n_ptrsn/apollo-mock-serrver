const { ApolloServer, gql } = require("apollo-server");

// Define GQL query types
const typeDefs = gql`
  type Query {
    message: String!
  }
`;

// Create Apollo mock server instance
const server = new ApolloServer({
  typeDefs,
  mocks: true,
});

// Start the mock server
server.listen().then(({ url }) => console.log(`Server running on ${url}`));
